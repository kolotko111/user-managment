﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UserManagment2.ViewModel
{
    public class UserViewModel
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public bool EmailConfirmed { get; set; }
        public string Role { get; set; }
    }
}