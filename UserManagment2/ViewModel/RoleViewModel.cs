﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UserManagment2.Models;

namespace UserManagment2.ViewModel
{
    public class RoleViewModel
    {
        public RoleViewModel() { }

        public RoleViewModel(ApplicationRole role)
        {
            Id = role.Id;
            Name = role.Name;
            IsCreateByUser = role.IsCreateByUser;
        }

        public string Id { get; set; }
        public string Name { get; set; }
        public bool IsCreateByUser { get; set; }
    }
}