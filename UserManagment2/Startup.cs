﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Owin;
using UserManagment2.Models;

[assembly: OwinStartupAttribute(typeof(UserManagment2.Startup))]
namespace UserManagment2
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            CreateRolesAndUsers();
        }

        // In this method we will create default User roles and Admin user for login   
        private void CreateRolesAndUsers()
        {
            ApplicationDbContext context = new ApplicationDbContext();

            var roleManager = new RoleManager<ApplicationRole>(new RoleStore<ApplicationRole>(context));
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));


            // In Startup iam creating first Admin Role and creating a default Admin User   
            if (!roleManager.RoleExists("Admin"))
            {

                // Create Admin rool   
                var role = new ApplicationRole();
                role.Name = "Admin";
                role.IsCreateByUser = false;
                roleManager.Create(role);
            }

            if (!roleManager.RoleExists("Student"))
            {

                // Create Student rool   
                var role = new ApplicationRole();
                role.Name = "Student";
                role.IsCreateByUser = true;
                roleManager.Create(role);
            }

            if (!roleManager.RoleExists("Promoter"))
            {
                // Create Promoter rool   
                var role = new ApplicationRole();
                role.Name = "Promoter";
                role.IsCreateByUser = true;
                roleManager.Create(role);
            }

            if (!roleManager.RoleExists("Reviewer"))
            {
                // Create Reviewer rool   
                var role = new ApplicationRole();
                role.Name = "Reviewer";
                role.IsCreateByUser = false;
                roleManager.Create(role);
            }

            //if admin does not exist, create admin
            if (userManager.FindByName("sanostrowtest@gmail.com") == null)
            {
                //Here we create a Admin super user who will maintain the website           
                var user = new ApplicationUser();
                user.UserName = "sanostrowtest@gmail.com";
                user.Email = "sanostrowtest@gmail.com";

                string userPassword = "ZAQ!2wsx";
                var userCreated = userManager.Create(user, userPassword);

                //Add default User to Role Admin   
                if (userCreated.Succeeded)
                {
                    var result1 = userManager.AddToRole(user.Id, "Admin");
                }
            }
        }
    }
}
