﻿using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using UserManagment2.Models;
using UserManagment2.ViewModel;

namespace UserManagment2.Controllers
{
    public class RoleController : Controller
    {
        private ApplicationRoleManager _roleManager;

        public RoleController()
        {
        }

        public RoleController(ApplicationRoleManager roleManager)
        {
            RoleManager = roleManager;
        }

        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }

        // GET: Role
        public ActionResult Index()
        {
            List<RoleViewModel> list = new List<RoleViewModel>();
            foreach (var role in RoleManager.Roles)
            {
                list.Add(new RoleViewModel(role));
            }
            return View(list);
        }

        // GET: Role/Details/5
        public async Task<ActionResult> Details(string id)
        {
            var role = await RoleManager.FindByIdAsync(id);
            return View(new RoleViewModel(role));
        }

        // GET: Role/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Role/Create
        [HttpPost]
        public async Task<ActionResult> Create(RoleViewModel model)
        {
            try
            {
                var role = new ApplicationRole() { Name = model.Name, IsCreateByUser = model.IsCreateByUser };
                await RoleManager.CreateAsync(role);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Role/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            var role = await RoleManager.FindByIdAsync(id);
            return View(new RoleViewModel(role));
        }

        // POST: Role/Edit/5
        [HttpPost]
        public async Task<ActionResult> Edit(RoleViewModel model)
        {
            var orginalRole = await RoleManager.FindByIdAsync(model.Id);
            orginalRole.Name = model.Name;
            orginalRole.IsCreateByUser = model.IsCreateByUser;
            // var role = new ApplicationRole() { Id = model.Id, Name = model.Name, IsCreateByUser = model.IsCreateByUser };

            await RoleManager.UpdateAsync(orginalRole);

            return RedirectToAction("Index");
        }

        // GET: Role/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            var role = await RoleManager.FindByIdAsync(id);
            return View(new RoleViewModel(role));
        }

        // POST: Role/Delete/5
        [HttpPost]
        public async Task<ActionResult> Delete(RoleViewModel model)
        {
            var role = await RoleManager.FindByIdAsync(model.Id);
            await RoleManager.DeleteAsync(role);

            return RedirectToAction("Index");
        }
    }
}