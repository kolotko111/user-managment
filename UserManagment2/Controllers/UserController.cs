﻿using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using UserManagment2.Models;
using UserManagment2.ViewModel;

namespace UserManagment2.Controllers
{
    public class UserController : Controller
    {
        private ApplicationUserManager _userManager;
        private ApplicationRoleManager _roleManager;

        public UserController()
        {
        }

        public UserController(ApplicationUserManager userManager, ApplicationRoleManager roleManager)
        {
            UserManager = userManager;
            RoleManager = roleManager;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }
        // GET: User
        public async Task<ActionResult> Index()
        {
            List<UserViewModel> userList = new List<UserViewModel>();

            foreach (var user in UserManager.Users.ToList())
            {
                var newRole = await UserManager.GetRolesAsync(user.Id);
                userList.Add(new UserViewModel()
                {
                    Id = user.Id,
                    Email = user.Email,
                    EmailConfirmed = user.EmailConfirmed,
                    Role = newRole.FirstOrDefault()
                });
            }

            IEnumerable<UserViewModel> IUserList = userList;

            return View(IUserList);

            #region
            //var usersWithRoles = (from user in context.Users
            //                      select new
            //                      {
            //                          UserId = user.Id,
            //                          Username = user.UserName,
            //                          Email = user.Email,
            //                          RoleNames = (from userRole in user.Roles
            //                                       join role in context.Roles on userRole.RoleId
            //                                       equals role.Id
            //                                       select role.Name).ToList()
            //                      }).ToList().Select(p => new Users_in_Role_ViewModel()

            //                      {
            //                          UserId = p.UserId,
            //                          Username = p.Username,
            //                          Email = p.Email,
            //                          NewRole = string.Join(",", p.RoleNames)
            //                      });
            #endregion
        }

        // GET: User/Details/5
        public async Task<ActionResult> Details(string id)
        {
            var user = await UserManager.FindByIdAsync(id);
            var role = await UserManager.GetRolesAsync(user.Id);
            return View(new UserViewModel() { Email = user.Email, EmailConfirmed = user.EmailConfirmed, Role = role.FirstOrDefault() });
        }

        // GET: User/Create
        public ActionResult Create()
        {
            //display all roles
            List<SelectListItem> list = new List<SelectListItem>();
            foreach (var role in RoleManager.Roles)
            {
                list.Add(new SelectListItem() { Value = role.Name, Text = role.Name });
            }
            ViewBag.Roles = list;

            return View();
        }

        // POST: User/Create
        [HttpPost]
        public async Task<ActionResult> Create(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    result = await UserManager.AddToRoleAsync(user.Id, model.RoleName);
                }
            }
            return RedirectToAction("Index");
        }

        // GET: User/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            //display all roles
            List<SelectListItem> list = new List<SelectListItem>();
            foreach (var role in RoleManager.Roles)
            {
                list.Add(new SelectListItem() { Value = role.Name, Text = role.Name });
            }
            ViewBag.Roles = list;


            var user = await UserManager.FindByIdAsync(id);
            var Role = await UserManager.GetRolesAsync(user.Id);


            return View(new UserViewModel() { Id = user.Id, Email = user.Email, Role = Role.First() });
        }

        // POST: User/Edit/5
        [HttpPost]
        public async Task<ActionResult> Edit(UserViewModel model)
        {
            //var user = new ApplicationUser() { Id = model.Id, Email = model.Email, UserName = model.Email };

            var orginalUser = await UserManager.FindByIdAsync(model.Id);
            orginalUser.Email = model.Email;
            orginalUser.UserName = model.Email;

            //We cant update user if user is the same like orginal user 
            //Update user
            var resoult2 = await UserManager.UpdateAsync(orginalUser);
            if (!resoult2.Succeeded)
            {
                //return error
            }


            //If we dont use auxyliary list we get error 
            var listOfRoles = RoleManager.Roles.ToList();
            foreach (var role in listOfRoles)
            {
                //Check that user have this role 
                var isInRole = await UserManager.IsInRoleAsync(model.Id, role.Name);
                if (isInRole)
                    await UserManager.RemoveFromRoleAsync(model.Id, role.Name);
            }

            //Add user to role 
            var resoult = await UserManager.AddToRoleAsync(model.Id, model.Role);
            if (!resoult.Succeeded)
            {
                //return error
            }

            return RedirectToAction("Index");
        }

        // GET: User/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            var user = await UserManager.FindByIdAsync(id);
            var Role = await UserManager.GetRolesAsync(user.Id);
            return View(new UserViewModel() { Id = user.Id, Email = user.Email, Role = Role.FirstOrDefault() });
        }

        // POST: User/Delete/5 
        [HttpPost]
        public async Task<ActionResult> Delete(UserViewModel model)
        {
            var user = await UserManager.FindByIdAsync(model.Id);
            await UserManager.DeleteAsync(user);

            return RedirectToAction("Index");
        }
    }
}