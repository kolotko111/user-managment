namespace UserManagment2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FormModels", "PromoterId", c => c.Int(nullable: false));
            AddColumn("dbo.FormModels", "Promoter_Id", c => c.String(maxLength: 128));
            CreateIndex("dbo.FormModels", "Promoter_Id");
            AddForeignKey("dbo.FormModels", "Promoter_Id", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FormModels", "Promoter_Id", "dbo.AspNetUsers");
            DropIndex("dbo.FormModels", new[] { "Promoter_Id" });
            DropColumn("dbo.FormModels", "Promoter_Id");
            DropColumn("dbo.FormModels", "PromoterId");
        }
    }
}
