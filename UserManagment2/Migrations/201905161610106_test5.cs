namespace UserManagment2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test5 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.FormModels", "Promoter_Id", "dbo.AspNetUsers");
            AddColumn("dbo.FormModels", "ApplicationUser_Id", c => c.String(maxLength: 128));
            AddColumn("dbo.FormModels", "ApplicationUser_Id1", c => c.String(maxLength: 128));
            AddColumn("dbo.AspNetUsers", "FormModel_FormId", c => c.Int());
            CreateIndex("dbo.FormModels", "ApplicationUser_Id");
            CreateIndex("dbo.FormModels", "ApplicationUser_Id1");
            CreateIndex("dbo.AspNetUsers", "FormModel_FormId");
            AddForeignKey("dbo.FormModels", "ApplicationUser_Id", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.AspNetUsers", "FormModel_FormId", "dbo.FormModels", "FormId");
            AddForeignKey("dbo.FormModels", "ApplicationUser_Id1", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FormModels", "ApplicationUser_Id1", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUsers", "FormModel_FormId", "dbo.FormModels");
            DropForeignKey("dbo.FormModels", "ApplicationUser_Id", "dbo.AspNetUsers");
            DropIndex("dbo.AspNetUsers", new[] { "FormModel_FormId" });
            DropIndex("dbo.FormModels", new[] { "ApplicationUser_Id1" });
            DropIndex("dbo.FormModels", new[] { "ApplicationUser_Id" });
            DropColumn("dbo.AspNetUsers", "FormModel_FormId");
            DropColumn("dbo.FormModels", "ApplicationUser_Id1");
            DropColumn("dbo.FormModels", "ApplicationUser_Id");
            AddForeignKey("dbo.FormModels", "Promoter_Id", "dbo.AspNetUsers", "Id");
        }
    }
}
