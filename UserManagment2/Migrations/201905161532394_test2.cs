namespace UserManagment2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FormModels", "Author_Id", c => c.String(nullable: false, maxLength: 128));
            CreateIndex("dbo.FormModels", "Author_Id");
            AddForeignKey("dbo.FormModels", "Author_Id", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FormModels", "Author_Id", "dbo.AspNetUsers");
            DropIndex("dbo.FormModels", new[] { "Author_Id" });
            DropColumn("dbo.FormModels", "Author_Id");
        }
    }
}
