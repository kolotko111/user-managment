﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UserManagment2.Models
{
    public class FileModel
    {
        public int FileId { get; set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public byte[] File { get; set; }

        //Relation One to One with our Form
        public virtual FormModel FormModel { get; set; }
    }
}