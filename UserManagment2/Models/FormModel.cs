﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UserManagment2.Models
{
    public class FormModel
    {
        public FormModel()
        {
            this.Reviewer = new HashSet<ApplicationUser>();
        }

        public int FormId { get; set; }
        public string Name { get; set; }

        //Relation One to One with our PDF
        public virtual FileModel FileModel { get; set; }
        //Relation One to One betwean Author and form
        public virtual ApplicationUser Author { get; set; }
        //Relation one to many betwean Promotor and form
        public int PromoterId { get; set; }
        public ApplicationUser Promoter { get; set; }

        public ICollection<ApplicationUser> Reviewer { get; set; }

    }
}