﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace UserManagment2.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        //Form for user
        public virtual FormModel YourForm { get; set; }
        //Forms for promotor
        public ICollection<FormModel> PromotorOfForm { get; set; }
        //Forms for review
        public ApplicationUser() 
        {
            this.FormForReview = new HashSet<FormModel>();
        }

        public ICollection<FormModel> FormForReview { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationRole : IdentityRole
    {
        public ApplicationRole() : base() { }
        public ApplicationRole(string roleName) : base(roleName) { }
        public bool IsCreateByUser { get; set; }

    }


    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //Without this line we cant migrating
            base.OnModelCreating(modelBuilder);

            #region
            // Relation One to One betwean PDF and Form
            modelBuilder.Entity<FileModel>()
                        .HasKey(q => q.FileId);

            modelBuilder.Entity<FormModel>()
                        .HasKey(q => q.FormId)
                        .HasRequired(w => w.FileModel)
                        .WithRequiredPrincipal(e => e.FormModel);
            #endregion

            #region
            //Relation One to zero or one betwean user and form
            //This relation depend for create form for user Author => Your Form
            modelBuilder.Entity<ApplicationUser>()
                        .HasOptional(q => q.YourForm) 
                        .WithRequired(w => w.Author);
            #endregion


        }

        public DbSet<FormModel> FormModel { get; set; }
        public DbSet<FileModel> FileModel { get; set; }

    }
}